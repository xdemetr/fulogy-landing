import React, { useContext } from 'react';
import ThemeContext from '../../data/context';
import Section from '../Section';
import List from '../List';

import './SectionWorks.sass';

const SectionWorks = () => {

  const { sectionWorks } = useContext(ThemeContext);

  const works = sectionWorks.works.map(({ id, title, href, details, price }) => {
    return (
      <li className="list__item work-preview" key={id}>
        <img src={require(`./works/${id}.jpg`)} alt={title} className="work-preview__img"/>

        <h3 className="work-preview__title">
          <a href={href} className="work-preview__link">{title}</a>
        </h3>
        <List items={details} type="work-preview" mix="work-preview__details"/>
        <p className="work-preview__price">
          Цена: <span className="work-preview__price-value">{price}</span>
        </p>
      </li>
    )
  });

  return (
    <Section type="works">
      <header className="section__header">
        <h2 className="section__title">{sectionWorks.title}</h2>
      </header>

      <div className="section__content">
        <ul className="list list_type_works">
          {works}
        </ul>
      </div>
    </Section>
  );
};

export default SectionWorks;
