import React, { useState } from 'react';

import './ColorSelect.sass';

import worker from './light-select__image.png';
import white from './white.png';
import gold from './gold.png';
import black from './black.png';
import silver from './silver.png';

const ColorSelect = ({ items }) => {

  const [color, setColor] = useState(gold);

  const colorChange = (type) => {
    switch (type) {
      case 'gold':
        setColor(gold);
        break;

      case 'black':
        setColor(black);
        break;

      case 'white':
        setColor(white);
        break;

      case 'silver':
        setColor(silver);
        break;
    }
  };

  const colorSelectListItems = items.map(({ id, type, label }) => {
    return (
      <li
        key={id}
        title={label}
        className={`select-switch__item select-switch__item_type_${type}`}
        onClick={() => colorChange(type)}
      >
        {label}
      </li>
    )
  });

  return (
    <div className="light-select">
      <div className="light-select__switch select-switch">
        <span className="select-switch__label">Выберите цвет светильника</span>

        <ul className="select-switch__list">
          {colorSelectListItems}
        </ul>
      </div>

      <img className="light-select__image" src={worker} alt=""/>

      <img className="light-select__hero-image" src={color} alt=""/>

    </div>
  );
};

export default ColorSelect;
