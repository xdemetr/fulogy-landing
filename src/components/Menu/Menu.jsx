import React from 'react';
import cn from 'classnames';

import './Menu.sass';
import SvgIcon from '../SvgIcon';

const Menu = ({ type, items, tag, mix }) => {
  if (!items) return null;

  const Tag = tag ? tag : 'ul';

  const renderItems = items.map(({ id, href, label, icon }) => {
    return (
      <li className="menu__item" key={id}>
        <a
          href={href}
          className={cn('menu__link', icon && 'menu__link_with-icon')}>
          {icon && <SvgIcon content={icon} mix="menu__icon"/>}
          <span className="menu__text">{label}</span>
        </a>
      </li>
    )
  });

  return (
    <Tag className={cn('menu', type && `menu_type_${type}`, mix && mix)}>
      {renderItems}
    </Tag>
  );
};

export default Menu;
