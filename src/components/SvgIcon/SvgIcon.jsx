import React from 'react';
import cn from 'classnames';

import { ReactComponent as Speed } from './content/speed.svg';
import { ReactComponent as Policy } from './content/policy.svg';
import { ReactComponent as Tap } from './content/tap.svg';
import { ReactComponent as BrightStars } from './content/bright-stars.svg';
import { ReactComponent as PriceArrow } from './content/price-arrow.svg';
import { ReactComponent as WhatsApp } from './content/whatsapp.svg';
import { ReactComponent as Telegram } from './content/telegram.svg';
import { ReactComponent as Viber } from './content/viber.svg';
import { ReactComponent as InvertedCommas } from './content/inverted-commas.svg';

import './SvgIcon.sass';

const SvgIcon = ({ content, mix }) => {

  let icon = '';

  switch (content) {
    case 'speed':
      icon = <Speed/>;
      break;

    case 'policy':
      icon = <Policy/>;
      break;

    case 'tap':
      icon = <Tap/>;
      break;

    case 'bright-stars':
      icon = <BrightStars/>;
      break;

    case 'price-arrow':
      icon = <PriceArrow/>;
      break;

    case 'whatsapp':
      icon = <WhatsApp/>;
      break;

    case 'telegram':
      icon = <Telegram/>;
      break;

    case 'viber':
      icon = <Viber/>;
      break;

    case 'inverted-commas':
      icon = <InvertedCommas/>;
      break;

    default:
      return null;
  }

  return (
    <div className={cn('svg-icon', content && `svg-icon_content_${content}`, mix)}>
      {icon}
    </div>
  );
};

export default SvgIcon;
