import React from 'react';
import cn from 'classnames';

import './Logo.sass';

const Logo = ({ mix }) => {
  return (
    <div className={cn('logo', mix)}>
      <a href={"#"} className="logo__link">Fulogy</a>
    </div>
  );
};

export default Logo;
