import React, { useContext } from 'react';
import Navigation from '../Navigation';
import ThemeContext from '../../data/context';

import './AppHeader.sass';
import Logo from '../Logo';

const AppHeader = () => {
  const { mainMenu, general } = useContext(ThemeContext);

  return (
    <div className="main-header">
      <div className="main-header__container">
        <Logo mix="main-header__logo"/>
        <Navigation items={mainMenu} mix="main-header__navigation"/>

        <div className="main-header__aside">
          <a href="#" className="main-header__callback-link">Заказать звонок</a>
          <a href={general.phoneLink} className="main-header__phone-link">{general.phone}</a>
        </div>
      </div>
    </div>
  );
};

export default AppHeader;
