import React from 'react';

import cn from 'classnames';
import Menu from '../Menu';

const Navigation = ({ items, mix }) => {
  if (!items) return null;

  return (
    <nav className={cn('navigation', mix)}>
      <Menu items={items} type="main" />
    </nav>
  );
};

export default Navigation;
