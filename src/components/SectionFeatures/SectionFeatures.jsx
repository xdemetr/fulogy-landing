import React, { useContext } from 'react';
import Section from '../Section';
import ThemeContext from '../../data/context';
import List from '../List';
import Carousel from '../Carousel';

import './SectionFeatures.sass';

const SectionFeatures = () => {

  const { sectionFeatures } = useContext(ThemeContext);

  return (
    <Section type="features">
      <div className="section__content">
        <div className="section__swiper">
          <Carousel items={sectionFeatures.carousel} mix="swiper_for_features"/>
        </div>

        <List items={sectionFeatures.features} type="features" mix="section__features-list"/>
      </div>
    </Section>
  );
};

export default SectionFeatures;
