import React from 'react';
import cn from 'classnames';
import SvgIcon from '../SvgIcon';

import './List.sass';

const List = ({ tag, type, items, mix }) => {

  if (!items) return null;


  const Tag = tag ? tag : 'ul';

  const listItems = items.map(({ id, content, icon }) => {
    return (
      <li className="list__item" key={id}>
        {icon && <SvgIcon content={icon} mix="list__icon"/>}
        <span className="list__content">{content}</span>
      </li>
    )
  });

  return (
    <Tag className={cn('list', type && `list_type_${type}`, mix && mix)}>
      {listItems}
    </Tag>
  );
};

export default List;
