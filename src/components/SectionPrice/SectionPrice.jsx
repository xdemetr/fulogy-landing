import React, { useContext } from 'react';
import ThemeContext from '../../data/context';
import Section from '../Section';

import './SectionPrice.sass';
import List from '../List';
import Button from '../Forms/Button';
import SvgIcon from '../SvgIcon';
import Menu from '../Menu';

const SectionPrice = () => {
  const { sectionPrice, general } = useContext(ThemeContext);

  return (
    <Section type="price">
      <header className="section__header">
        <h2 className="section__title">{sectionPrice.title}</h2>

        <p className="section__description">Стоимость погонного метра светильника fulogy
          от<br/><strong>2000</strong> руб.
        </p>
      </header>

      <div className="section__content">
        <h3 className="section__content-title">
          В цену входит:
          <SvgIcon content="price-arrow" mix="section__content-title-img"/>
        </h3>
        <List items={sectionPrice.features} type="features-price"/>
      </div>

      <div className="section__footer">
        <div className="section__main-action">
          <Button type="primary" wide={true}>Заказать звонок</Button>
        </div>
        <p className="section__contacts">
          или звоните по телефону:
          <br/>
          <a className="section__phone-link" href={general.phoneLink}>{general.phone}</a>
        </p>

        <Menu items={general.social} type="social"/>

      </div>

      <div className="section__decorate">
        <SvgIcon content="inverted-commas"/>
      </div>
    </Section>
  );
};

export default SectionPrice;
