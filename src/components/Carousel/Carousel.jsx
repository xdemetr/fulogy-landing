import React from 'react';
import Swiper from 'react-id-swiper';

import 'swiper/css/swiper.css';
import './Carousel.sass';

const Carousel = ({ items, mix }) => {

  const params = {
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.swiper__button-next',
      prevEl: '.swiper__button-prev'
    }
  };

  const renderItems = items.map(({ id, image }) => {

    const ImageBox = () => {
      if (!image) return null;

      return (
        <div className="swiper__image-box">
          <img src={require(`./features/${image}`)} alt="" className="swiper__image"/>
        </div>
      )
    };

    return (
      <div key={id}>
        <ImageBox/>
      </div>
    )
  });

  return (
    <Swiper containerClass={mix} {...params}>
      {renderItems}
    </Swiper>
  );
};

export default Carousel;
