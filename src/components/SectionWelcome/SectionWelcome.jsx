import React, { useContext } from 'react';
import Button from '../Forms/Button';
import Section from '../Section';

import './SectionWelcome.sass';
import ColorSelect from '../ColorSelect';
import List from '../List';
import ThemeContext from '../../data/context';


const SectionWelcome = () => {

  const { sectionWelcome } = useContext(ThemeContext);

  return (
    <Section type="welcome">
      <header className="section__header">
        <h2 className="section__title">{sectionWelcome.title}</h2>

        <p className="section__description"><strong>8 лет</strong> оснащаем Ваши кухни нашими светильниками по цене
          производителя.</p>

        <div className="section__video-preview">
          <div className="section__video-preview-text">
            <strong>Посмотрите</strong> видеопрезентацию о&nbsp;наших светильниках!
          </div>
          <div className="section__video-preview-action">
            <Button type="video-preview">Посмотреть презентацию</Button>
          </div>
        </div>

        <div className="section__main-action">
          <Button type="primary">Онлайн конструктор</Button>
        </div>
      </header>

      <div className="section__content">
        <div className="section__lightning">
          <ColorSelect items={sectionWelcome.colorSelect}/>
        </div>
      </div>

      <div className="section__footer">
        <List type="welcome-features" items={sectionWelcome.features}/>
      </div>
    </Section>
  );
};

export default SectionWelcome;
