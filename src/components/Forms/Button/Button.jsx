import React from 'react';
import cn from 'classnames';

import './Button.sass';

const Button = ({type, htmlType, wide, ...props}) => {

  const Tag = props.href ? "a" : "button";

  return (
    <Tag className={cn(
      'button',
      type && `button_type_${type}`,
      wide && 'button_wide'
    )}>
      {props.children}
    </Tag>
  );
};

export default Button;
