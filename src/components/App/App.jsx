import React from 'react';
import AppHeader from '../AppHeader';
import ThemeContext from '../../data/context';

import SectionWelcome from '../SectionWelcome';
import SectionPrice from '../SectionPrice';
import SectionWorks from '../SectionWorks';
import SectionFeatures from '../SectionFeatures';

import general from '../../data/general';
import mainMenu from '../../data/mainMenu';
import sectionWelcome from '../../data/sectionWelcome';
import sectionPrice from '../../data/sectionPrice';
import sectionWorks from '../../data/sectionWorks';
import sectionFeatures from '../../data/sectionFeatures';

const data = {
  general,
  mainMenu,
  sectionWelcome,
  sectionPrice,
  sectionWorks,
  sectionFeatures
};

const App = () => {

  return (
    <ThemeContext.Provider value={data}>
      <div className="app">
        <AppHeader/>

        <SectionWelcome/>

        <SectionPrice/>

        <SectionWorks/>

        <SectionFeatures/>
      </div>
    </ThemeContext.Provider>
  );
};

export default App;
