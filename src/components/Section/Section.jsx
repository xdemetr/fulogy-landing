import React from 'react';
import cn from 'classnames';

import './Section.sass';

const Section = ({ type, ...props }) => {


  return (
    <section className={cn('section', type && `section_type_${type}`)}>
      <div className="section__container">
        {props.children}
      </div>
    </section>
  );
};

export default Section;
